package com.example.demo.domain;

import lombok.*;

/**
 * @Description User
 * @Date 2020/7/6 13:17
 * @Version 1.0
 **/
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class User {
    private Long id;
    private String name;
    private Integer age;
}
